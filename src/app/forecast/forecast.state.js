import axios from 'axios'
export const GET_CLOUD_COVERAGE = 'GET_CLOUD_COVERAGE'
export const GET_CLOUD_COVERAGE_SUCCESS = 'GET_CLOUD_COVERAGE_SUCCESS'
export const GET_SOLAR_ACTIVITY = 'GET_SOLAR_ACTIVITY'
export const GET_SOLAR_ACTIVITY_SUCCESS = 'GET_SOLAR_ACTIVITY_SUCCESS'

const createURL = type => {
	let date = new Date()
	const utc = date.getTime() - (date.getTimezoneOffset() * 60000)
	date = new Date(utc + 3600 * 1000 * -3)
	return `http://api.planetos.com/v1/datasets/bom_access-g_global_40km/point?
					lat=59.4&lon=24.7&apikey=652f8edd391f4e5d90b9654a7f4c1bc0&
					var=${type}&json=true&count=10&start=${date.toISOString()}`
}
export const getCloudCoverage = () => {
	return dispatch => {
		dispatch({
			type: GET_CLOUD_COVERAGE
		})
		axios.get(createURL('av_ttl_cld'))
			.then(payload => {
			dispatch({
				type: GET_CLOUD_COVERAGE_SUCCESS,
				info: payload.data.entries
			})
		})
	}
}
export const getSolarActivity = () => {
	return dispatch => {
		dispatch({
			type: GET_SOLAR_ACTIVITY
		})
		axios.get(createURL('av_swsfcdown'))
			.then(payload => {
				dispatch({
					type: GET_SOLAR_ACTIVITY_SUCCESS,
					info: payload.data.entries
				})
			})
	}
}


export function forecastReducer(state = {}, {type, info}) {
	switch(type) {
		case GET_CLOUD_COVERAGE : {
			return state
		}
		case GET_CLOUD_COVERAGE_SUCCESS : {
			return {...state,	cloudCoverage: info }
		}
		case GET_SOLAR_ACTIVITY: {
			return state
		}
		case GET_SOLAR_ACTIVITY_SUCCESS: {
			return {...state,	solarActivity: info }
		}
		default : {
			return state
		}
	}
}