import React, { Component } from 'react'
import { connect } from 'react-redux'
import ui from '../common/theme'
import styled from 'styled-components'
import Graph from './Graph'
import {
	getSolarActivity,
	getCloudCoverage
} from './forecast.state'


const Container = styled.div`
	background-color: ${ui.color.black};
	margin: 0 -${ui.size.s};
	display: flex;
	min-width: 100vw;
	flex-wrap: wrap;	
	padding: ${ui.size.s} ${ui.size.m};
	border-bottom: ${ui.border.primary};	
	section {
		min-width: 300px;
		background-color: ${ui.color.background};
		flex: 1;
		border-radius: ${ui.size.xs};
		padding: ${ui.size.s};
		padding-top: ${ui.size.l};
		margin: ${ui.size.xs};
		position: relative;
		.top, .bottom {
			position: absolute;
			font-size: ${ui.size.s};
			right: ${ui.size.m};
			color: ${ui.color.accent}
		}
		.top {
			top: ${ui.size.ms};
		}
		.bottom {
			bottom: ${ui.size.m};
		}
	}
	.spec, .top, .bottom {
		font-weight: bold;
	}
	svg {
		border-radius: ${ui.size.xs};
		width: 100%;
		background-color: ${ui.color.black};
	}
	.spec {			
			font-size: ${ui.size.ms};
			color: ${ui.color.content};
			margin-left: ${ui.size.xs}
		}
`

class Forecast extends Component {
	componentWillMount() {
		this.getForecasts()
		this.pingAPIs()
	}

	getForecasts() {
		this.props.dispatch(getSolarActivity())
		this.props.dispatch(getCloudCoverage())
	}

	pingAPIs() {
		setInterval(() => this.getPanelUpdates(), 5 * 60 * 1000)
	}
	render() {
		const {solarActivity, cloudCoverage} = this.props.forecast
		return (
			<Container>
				<Graph
					topText="1000 W/m2"
					bottomText="0 W/m2"
					specText="SOLAR ACTIVITY"
					fillColor="#C82C2A"
					dataVariable="av_swsfcdown"
					scale={0.1}
					forecast={solarActivity} />
				<Graph
					topText="CLEAR 100%"
					bottomText="CLOUDY 0%"
					specText="CLOUD COVERAGE"
					fillColor="#257B9C"
					dataVariable="av_ttl_cld"
					scale={100}
					forecast={cloudCoverage} />
			</Container>
		);
	}
}

const mapStateToProps = state => ({
	forecast: state.forecast
})

export default connect(mapStateToProps)(Forecast)