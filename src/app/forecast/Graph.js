import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ui from '../common/theme'
import { select } from 'd3-selection'
import { attrs } from 'd3-selection-multi'
import { scaleTime } from 'd3-scale'
import { axisBottom } from 'd3-axis'
import { timeFormat } from 'd3-time-format'

class Graph extends Component {
	constructor(props) {
		super(props)
		this.createComponent = this.createComponent.bind(this)
	}
	componentDidMount() {
		this.createComponent()
		window.addEventListener('resize', this.createComponent)
	}
	componentDidUpdate() {
		this.createComponent()
	}
	componentWillUnmount() {
		window.removeEventListener('resize', this.createComponent)
	}
	createComponent() {
		const {
			fillColor,
			dataVariable,
			forecast,
			scale } = this.props
		const node = this.node
		const dataset = forecast
				.slice(0,10)
				.map((item, i) => {
					return {
						time: item.axes.time,
						value: (item.data[dataVariable] > 0) ? item.data[dataVariable] : 0
					}
				}),
			width = this.node.clientWidth,
			height = 130,
			barWidth = width / dataset.length,
			padding = 1,
			margin = 20

		const getDomain = (data) => {
			return data[0]
				? [new Date(dataset[0].time), new Date(dataset[dataset.length - 1].time)]
				: [0, 0]
		}
		const xScale = scaleTime()
			.domain(getDomain(dataset))
			.range([margin,width + margin])
		const xAxis = axisBottom(xScale).tickFormat(timeFormat('%H:%M%'))

		select(node).attrs({ width, height})

		select(node)
			.selectAll('rect')
			.data(dataset)
			.enter()
			.append('rect')
			.attrs({
				y: d => height - d.value * scale,
				height: d => d.value * scale,
				fill: fillColor,
				stroke: 'none'
			})

		select(node)
			.selectAll('text')
			.data(dataset)
			.enter()
			.append('text')
			.text(d => ((scale < 100) ? d.value : d.value * scale).toFixed(1))
			.attrs({
				class: 'label',
				y: d => height - d.value * scale - 5,
				fill: ui.color.accent,
				'font-size': ui.size.s,
				'font-weight': 'bold',
				'text-anchor': 'middle',
				stroke: 'none'
			})

		select(node).call(xAxis)
			.attrs({
				stroke: ui.color.content,
				'font-size': '9px'
			})

		select(this.node)
			.selectAll('rect')
			.attrs({
				x: (d,i) => i * barWidth,
				width: barWidth - padding,
			})

		select(this.node)
			.selectAll('text.label')
			.attrs({
				x: (d,i) => i * barWidth  + barWidth / 2,
			})
	}
	render() {
		const {topText, bottomText, specText} = this.props
		return (
			<section>
				<p className="top">{topText}</p>
				<svg ref={node => this.node = node} forecast={this.props.forecast}></svg>
				<p className="bottom">{bottomText}</p>
				<p className="spec">{specText}</p>

			</section>
		)
	}
}
Graph.propTypes = {
	forecast: PropTypes.array.isRequired,
	topText: PropTypes.string.isRequired,
	bottomText: PropTypes.string.isRequired,
	specText: PropTypes.string.isRequired,
	fillColor: PropTypes.string.isRequired,
	dataVariable: PropTypes.string.isRequired,
	scale: PropTypes.number.isRequired
}

export default Graph