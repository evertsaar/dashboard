import { combineReducers } from "redux"
import { farmReducer as farm } from "../farm/farm.state"
import { forecastReducer as forecast } from '../forecast/forecast.state'

export default combineReducers({
	farm,
	forecast
})