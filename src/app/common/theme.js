const size = {
	xs: '0.4rem',
	s: '1rem',
	ms: '1.4rem',
	m: '1.7rem',
	ml: '2.2rem',
	l: '2.8rem',
	xl: '3.4rem'
}

const color = {
	background: '#282828',
	content: '#E8E8E8',
	accent: '#F4A840',
	gray: '#3d3d3d',
	black: '#000',
	blue: 'rgba(125, 156, 169, 0.4)'
}

const font = {
	family: 'Arial'
}

const border = {
	primary: `${size.xs} solid ${color.accent}`
}

export default {
	color,
	size,
	font,
	border
}