import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ui from '../common/theme'

const style = {
	root: {
		flex: 1,
		minWidth: '180px',
		fontSize: ui.size.m,
		margin: '4px'
	},
	container: {
		boxSizing: 'content-box',
		backgroundColor: ui.color.background,
		lineHeight: 1,
		display: 'flex',
		padding: ui.size.s
	},
	id: {
		color: ui.color.accent,
		fontWeight: 'bold',
		padding: '5px',
		display: 'flex',
		alignItems: 'center'
	},
	name: {
		paddingBottom: ui.size.xs,
		display: 'inline'
	},
	stats: {
		flex: 1,
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'center',
		textAlign: 'right',
	},
	wattage: {
		fontWeight: 'bold',
		fontSize: ui.size.m,
		marginBottom: ui.size.xs
	},
	voltage: {
		borderTop: '2px solid ' + ui.color.gray,
		paddingTop: ui.size.xs,
		lineHeight: ui.size.m,
		fontSize: ui.size.ms,
		color: 'gray'
	}
}

const Panel = ({id, wattage, voltage}) => {
	return (
		<div style={style.root}>
			<div style={style.container}>
				<div style={style.id}>
					<p style={style.name}>{id}</p>
				</div>
				<div style={style.stats}>
					<p style={style.wattage}>{wattage}W</p>
					<p style={style.voltage}>{voltage}V</p>
				</div>
			</div>
		</div>
	)
}

Panel.propTypes = {
	id: PropTypes.string.isRequired,
	wattage: PropTypes.number.isRequired,
	voltage: PropTypes.number.isRequired
}

export default Panel