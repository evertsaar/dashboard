import queryMockServer from '../../api/server'

export const GET_PANEL_UPDATES = 'GET_PANEL_UPDATES'
export const GET_PANEL_UPDATES_SUCCESS = 'GET_PANEL_UPDATES_SUCCESS'
export const UPDATE_ENERGY_TOTAL = 'UPDATE_ENERGY_TOTAL'

export const getPanelUpdates = () => {
	return dispatch => {
		dispatch({
			type: GET_PANEL_UPDATES
		})
		queryMockServer().then(info => {
			dispatch({
				type: GET_PANEL_UPDATES_SUCCESS,
				info
			})
			dispatch({
				type: UPDATE_ENERGY_TOTAL
			})
		})
	}
}

export function farmReducer(state = {}, {type, info}) {
	switch (type) {
		case GET_PANEL_UPDATES: {
			return state
		}
		case GET_PANEL_UPDATES_SUCCESS: {
			return {...state,
				panels: info }
		}
		case UPDATE_ENERGY_TOTAL: {
			let total = state.panels.reduce((sum, panel) => sum + panel.wattage, 0)
			return {...state,
				totalKW: (total / 1000).toFixed(2)
			}
		}
		default : {
			return state
		}
	}
}