import React, { Component } from 'react'
import { connect } from 'react-redux'
import ui from '../common/theme'
import Panel from './Panel'
import { getPanelUpdates } from './farm.state'
import styled from 'styled-components'

const Container = styled.section`
		background-color: ${ui.color.black};
		flex: 1;
		border: 10px solid black;
		border-bottom-radius: ${ui.size.s};
`

const style = {
	list: {
		overflow: 'hidden',
		display: 'flex',
		flexWrap: 'wrap',
	},
	powerometer: {
		margin: '0 ' + ui.size.xs,
		display: 'flex',
		alignItems: 'center',
		fontSize: ui.size.m,
		color: ui.color.accent,
		fontWeight: 'bold',
		background: 'black',
	},
	totalKW: {
		margin: '0 3px',
		fontSize: ui.size.l,
		color: ui.color.content
	}
}

class Farm extends Component {
	componentWillMount() {
		this.getPanelUpdates()
		this.pingServer()
	}

	getPanelUpdates() {
		this.props.dispatch(getPanelUpdates())
	}

	pingServer() {
		setInterval(() => this.getPanelUpdates(), 10 * 1000)
	}

	getPanels(farm) {
		return farm.panels.map(panel => {
			return (
				<Panel
					key={panel.id}
					id={panel.id}
					voltage={panel.voltage}
					wattage={panel.wattage}
				/>
			)
		})
	}

	render() {
		return (
			<Container>
				<div style={style.powerometer}>
					<p>TOTAL</p>
					<p style={style.totalKW}>{this.props.farm.totalKW}</p>
					<p>kW</p>
				</div>
				<div style={style.list}>
					{this.getPanels(this.props.farm)}
				</div>
			</Container>
		);
	}
}

const mapStateToProps = state => ({
	farm: state.farm
})

export default connect(mapStateToProps)(Farm)