import React, { Component } from 'react'
import Forecast from './forecast/Forecast'
import Farm from './farm/Farm'
import styled from 'styled-components'

import './common/reset.css'
import ui from './common/theme'

const Spacing = styled.div`
	height: 100%;
	padding: ${ui.size.m} ${ui.size.s};
	font-size:  ${ui.size.xl};
	font-family: ${ui.font.family};
	color: ${ui.color.content};
	background-color: ${ui.color.background};
`
const Container = styled.section`
	background-color: ${ui.color.black};	
	display: flex;
	flex-direction: column;
	height: 100%;
	box-shadow: 0 0 10px #0f0f0f;
	overflow: hidden;
	border-radius: ${ui.size.s};	
`

class App extends Component {
	render() {
		return (
			<Spacing>
				<Container>
					<Forecast />
					<Farm />
				</Container>
			</Spacing>
		);
	}
}

export default App