import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import App from './app/App'
import store from './app/common/store'


const Dashboard = () => (
	<Provider store={store}>
		<App />
	</Provider>
)
render(<Dashboard />, document.getElementById('root'))