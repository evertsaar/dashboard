import shortid from 'shortid'
const farm = createFarm()

function createFarm() {
	const panels = []
	for (let i = 0; i < 30; i++) {
		panels.push({
			id: shortid.generate().substring(0, 5).replace(/(-|_)/g, '0').toUpperCase(),
			voltage: 0,
			wattage: 0
		})
	}
	return panels
}

const updateFarm = () => {
	return farm.map(panel => {
		const voltage = parseFloat((Math.random() * (40 - 10)).toFixed(2))
		const wattage = parseFloat((voltage * 3).toFixed(2))
		return {...panel, voltage, wattage }
	})
}

export default function () {
	return new Promise(resolve => {
		setTimeout(() => resolve(updateFarm()), 500)
	})
}